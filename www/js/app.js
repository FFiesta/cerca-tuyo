// Dom7
var $$ = Dom7;

// Framework7 App main instance
var app  = new Framework7({
    root: '#app', // App root element
    id: 'io.framework7.testapp', // App bundle ID
    name: 'Framework7', // App name
    theme: 'auto', // Automatic theme detection
    // App root data
    data: function () {
        return {
            user: {
                firstName: 'John',
                lastName: 'Doe',
            },
        };
    },
    // App root methods
    methods: {
        helloWorld: function () {
            app.dialog.alert('Hello World!');
        },
    },
    // App routes
    routes: routes,
});

// Init/Create main view
var mainView = app.views.create('.view-main', {
    url: '/'
});

// MAPBOX GL
/*
mapboxgl.accessToken = 'pk.eyJ1IjoidW5uZWNlc2FyeSIsImEiOiJjam9vcmhuMTEwYXVvM3FsNjJyamJoa2R2In0.wRiErCP3Ym48Y6NyObthOA';
const map = new mapboxgl.Map({
    container: 'map',
    style: 'mapbox://styles/unnecesary/cjouom4xb6jtp2rlqeu078cpv',
    center: [-58.519826, -34.531859],
    zoom: 17.1
});


map.addControl(new mapboxgl.NavigationControl());


map.on('click', 'cerca-tuyo', function (e) {
    var coordinates = e.features[0].geometry.coordinates.slice();
    var categoria = e.features[0].properties.categoria;

    // Ensure that if the map is zoomed out such that multiple
    // copies of the feature are visible, the popup appears
    // over the copy being pointed to.
    while (Math.abs(e.lngLat.lng - coordinates[0]) > 180) {
        coordinates[0] += e.lngLat.lng > coordinates[0] ? 360 : -360;
    }

    new mapboxgl.Popup()
        .setLngLat(coordinates)
        .setHTML(categoria)
        .addTo(map);
});*/
